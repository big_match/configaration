FROM cjpro/centosoraclejdk
ADD target/BMTConfigurationServer.jar //
ENTRYPOINT ["java", "-Xmx256m",  "-Xss256k", "-jar", "/BMTConfigurationServer.jar"]