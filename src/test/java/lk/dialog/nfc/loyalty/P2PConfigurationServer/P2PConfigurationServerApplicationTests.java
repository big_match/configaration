package lk.dialog.nfc.loyalty.P2PConfigurationServer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class P2PConfigurationServerApplicationTests {

	@Test
	public void contextLoads() {
	}

}
