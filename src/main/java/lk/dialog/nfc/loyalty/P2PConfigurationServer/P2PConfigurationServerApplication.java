package lk.dialog.nfc.loyalty.P2PConfigurationServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class P2PConfigurationServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(P2PConfigurationServerApplication.class, args);
	}
}
